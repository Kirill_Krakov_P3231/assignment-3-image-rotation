#include "rotate.h"

struct image rotate( struct image const source ) {
    struct image rotated = create_image(source.height, source.width);
    if (!rotated.data || !source.data) return rotated;
    for(size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            (rotated.data)[(source.width - 1 - j) * source.height + i] = (source.data)[i * source.width + j];
        }
    }
    return rotated;
}

