#include "image.h"
#include "bmp.h"
#include "file_bmp.h"
#include "rotate.h"
#include <string.h>

int main( int argc, char** argv ) {
    // проверка на корректность введённых аргументов
    if (argc != 4 || !strstr("-90-180-270",argv[3])) {
        fprintf(stderr, "Incorrect arguments");
        return 1;
    }

    // чтение в image информации из файла <source-image> и проверка на корректность чтения
    struct image image;
    enum read_status read_result = bmp_from_file(argv[1], &image);
    if (read_result != READ_OK) {
        fprintf(stderr, "Error reading file");
        destroy_image(&image);
        return 1;
    }

    // чтение заданного угла <angle> и преобразование отрицательных углов в их положительный аналог
    int64_t angle = strtol(argv[3], NULL, 10);
    if (angle < 0) { angle = 360 + angle; }

    // получение изображения rotated, повернутого на заданный угол
    struct image rotated;
    struct image rotated1;
    struct image rotated2;
    switch(angle) {
        case 0:
            rotated = create_image(image.width,image.height);
            if (!rotated.data) break;
            for (size_t i = 0; i < image.width * image.height; i++) {
                (rotated.data)[i] = (image.data)[i];
            }
            break;
        case 90:
            rotated = rotate(image);
            break;
        case 180:
            rotated1 = rotate(image);
            rotated = rotate(rotated1);
            destroy_image(&rotated1);
            break;
        case 270:
            rotated1 = rotate(image);
            rotated2 = rotate(rotated1);
            rotated = rotate(rotated2);
            destroy_image(&rotated1);
            destroy_image(&rotated2);
            break;
    }

    // запись информации из rotated в файл <transformed-image> и проверка на корректность записи
    enum write_status write_result = bmp_to_file(argv[2], &rotated);
    if (write_result != WRITE_OK) {
        fprintf(stderr, "Error writing file");
        destroy_image(&image);
        destroy_image(&rotated);
        return 1;
    }
    destroy_image(&image);
    destroy_image(&rotated);

    return 0;
}
