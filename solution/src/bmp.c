#include "bmp.h"

#define PIXEL_SIZE sizeof(struct pixel)
#define HEADER_SIZE sizeof(struct bmp_header)

// Функция для получения размера padding в байтах
size_t get_padding(uint32_t wight) {
    return (4 - (wight * PIXEL_SIZE) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;

    if (fread(&header, HEADER_SIZE, 1, in) < 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 19778) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    uint8_t* array = malloc(header.biSizeImage);
    if (!array) {
        return READ_INVALID_SIZE;
    }
    fread(array, header.biSizeImage, 1, in);
    if (ferror(in)) {
        return READ_INVALID_FILE;
    }

    size_t padding = get_padding(header.biWidth);
    size_t pixel_size = PIXEL_SIZE;
    *img = create_image(header.biWidth, header.biHeight);
    if (!img->data) return READ_INVALID_IMAGE;
    for (size_t i = 0; i < header.biHeight; i++) {
        for (size_t j = 0; j < header.biWidth; j++) {
            size_t x = i * (header.biWidth * pixel_size + padding) + j * pixel_size;
            struct pixel current = {array[x],array[x + 1],array[x + 2]};
            (img->data)[header.biWidth * i + j] = current;
        }
    }
    free(array);
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!img->data) return WRITE_ERROR;
    uint32_t  imageSize = (img->width * PIXEL_SIZE + get_padding(img->width)) * img->height;
    struct bmp_header header = {
            .bfType = 19778,
            .bfileSize = HEADER_SIZE + imageSize,
            .bfReserved = 0,
            .bOffBits = HEADER_SIZE,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = imageSize,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};

    if (fwrite(&header, sizeof(header), 1, out) < 1) {
        return WRITE_ERROR;
    }
    if (ferror(out)) {
        return WRITE_ERROR;
    }

    uint8_t* array = malloc(header.biSizeImage);
    size_t padding = get_padding(header.biWidth);
    size_t pixel_size = PIXEL_SIZE;
    for (size_t i = 0; i < header.biHeight; i++) {
        for (size_t j = 0; j < header.biWidth; j++) {
            size_t x = i * (img->width * pixel_size + padding) + j * pixel_size;
            size_t y = i * img->width + j;
            array[x] = (img->data)[y].b;
            array[x + 1] = (img->data)[y].g;
            array[x + 2] = (img->data)[y].r;
        }
        //заполняет padding для каждой строки
        for (size_t j = 0; j < padding; j++) {
            array[i * (img->width * pixel_size + padding) + img->width * pixel_size + j] = 0;
        }
    }

    if (fwrite(array, 1, header.biSizeImage, out) < header.biSizeImage) {
        free(array);
        return WRITE_ERROR;
    }
    free(array);
    return WRITE_OK;
}
