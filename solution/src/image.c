#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct image image = {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
    if (!image.data) {
        return (struct image) {0,0,NULL};
    }
    return image;
}

void destroy_image(struct image* image) {
    if (!(image->data)) { return; }
    free(image->data);
    image->height = 0;
    image->width = 0;
}
