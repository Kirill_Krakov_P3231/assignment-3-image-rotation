#include "file_bmp.h"
enum read_status bmp_from_file(char* filename, struct image* img) {
    FILE* in = fopen(filename, "rb");
    if (!in) { return READ_INVALID_FILE;}
    enum read_status result = from_bmp(in,img);
    fclose(in);
    return result;
}

enum write_status bmp_to_file(char* filename, struct image* img) {
    FILE* out = fopen(filename, "wb");
    if (!out) { return WRITE_ERROR; }
    enum write_status result = to_bmp(out,img);
    fclose(out);
    return result;
}
