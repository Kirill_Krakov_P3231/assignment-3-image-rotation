#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

// Функция для создания копии image, которая повёрнута на 90 градусов
struct image rotate( struct image const source );

#endif
