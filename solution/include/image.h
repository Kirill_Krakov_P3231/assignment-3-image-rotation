#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

// структура каждого пикселя размером 3 байта
struct pixel {
    uint8_t b, g, r;
};

// структура внутреннего представления картинки, очищенное от деталей формата
struct image {
    uint64_t width, height;
    struct pixel* data;
};

// функция для создания изображения - структуры image
struct image create_image(uint64_t width, uint64_t height);

// функция для деинициализации изображения - структуры image
void destroy_image(struct image* image);

#endif
