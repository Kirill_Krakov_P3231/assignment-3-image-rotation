#ifndef FILE_BMP_H
#define FILE_BMP_H

#include "bmp.h"

// Функция для открытия файла и запуска from_bmp
enum read_status bmp_from_file(char* filename, struct image* img);

// Функция для открытия файла и запуска to_bmp
enum write_status bmp_to_file(char* filename, struct image* img);

#endif
