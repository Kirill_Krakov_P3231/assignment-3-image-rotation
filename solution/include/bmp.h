#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

// структура заголовка bmp-файла для Microsoft
#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

// Коды для обработки чтения из BMP-файла
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_SIZE,
    READ_INVALID_IMAGE,
    READ_INVALID_FILE
};

// Коды для обработки записи в BMP-файл
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

// Функция для записи в структуру image информации из файла
enum read_status from_bmp(FILE *in, struct image *img);

// Функция для записи из структуры image информации в файл
enum write_status to_bmp( FILE* out, struct image const* img );

#endif
